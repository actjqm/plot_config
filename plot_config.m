function colors =  plot_config(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Nice plots!!!
%
% Just add 'colors = plot_config(width, height)' before you plot 
%
% To use the colors just follow the example:
% ---- plot(something, 'Color', colors.blue);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

width = 6.5;
height = 3;

switch nargin
    case 0
    case 1
        disp('Dude, did you just forget to input the width or height of your figure?')
    case 2
        width = varargin{1,1};     % Width in inches
        height = varargin{1,2};    % Height in inches
    case 3
        disp('Dude... Too many inputs!')
end

% Stuff
pos = [100, 100];
alw = 1;    % AxesLineWidth
fsz = 10;      % Fontsize
lw = 1.5;      % LineWidth

% Colors
colors.grey = [0.2, 0.2, 0.2];
colors.blue = [19, 58, 104]/255;
colors.green = [102, 142, 0]/255;
colors.red = [129, 36, 3]/255;

set(0,'DefaultAxesFontName', 'Arial')
set(0,'DefaultAxesFontSize', fsz)
set(0,'DefaultTextFontname', 'Arial')
set(0,'DefaultTextFontSize', fsz)
set(0, 'DefaultAxesColorOrder',[colors.blue; colors.green; colors.red; colors.grey])
set(0, 'DefaultAxesLineWidth', alw)
set(0, 'DefaultLineLineWidth', lw)
set(0, 'DefaultFigurePosition', [pos(1), pos(2), width*100, height*100])
set(0 ,'DefaultFigureColor','w');

disp('%% plot_config loaded %%');

end