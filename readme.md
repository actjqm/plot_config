Nice plots!!!
=============

Simple example function to make plots nicer. Edit away!

1. Just add 'colors = plot_config(width, height)' before you plot. (width and height in inches)

To use the colors just follow the example:
> plot(something, 'Color', colors.blue);